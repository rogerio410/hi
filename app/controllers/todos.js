import Ember from 'ember';

export default Ember.ArrayController.extend({
	todo: {},

	actions: {
		add:function(todo){
			//alert('add..'+todo.title);
			Ember.$.post('http://localhost:3000/todos',todo).then(function(){
				alert('Task added!');
			});
		},

		remove:function(id){
			Ember.$.ajax({
				url: 'http://localhost:3000/todos/'+id,
				type: 'delete',
				success: function(){
					alert('Task deleted!');
				}
			});
			
		}
	}
});
